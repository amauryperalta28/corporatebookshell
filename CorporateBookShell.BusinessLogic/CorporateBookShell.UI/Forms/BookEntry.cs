﻿using CorporateBookShell.BusinessLogic;
using CorporateBookShell.Models;
using System;
using System.Windows.Forms;

namespace CorporateBookShell.UI.Forms
{
    public partial class BookEntry : Form
    {
        private Bookshelf bookshelf;

        public BookEntry(Bookshelf bookshelf)
        {
            this.bookshelf = bookshelf;
            InitializeComponent();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ok_Click(object sender, EventArgs e)
        {
            var book = new Book(title.Text, isbn.Text, author.Text, int.Parse(year.Value.ToString()));

            bool isAdded = bookshelf.Add(book);

            ValidateAddedBook(isAdded);

            if (isAdded)
            {
                Clear();
            }

       

        }

        private void ValidateAddedBook(bool isAdded)
        {
            if (isAdded)
            {
                MessageBox.Show("Se agregó el libro.");

            }
            else
            {
                MessageBox.Show("Hubo un error al agregar el libro.");
            }
        }

        private void Clear()
        {
            foreach (Control control in this.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    textBox.Text = string.Empty;
                }
                else if (control is NumericUpDown)
                {
                    NumericUpDown numericUpDown = (NumericUpDown)control;
                    numericUpDown.Value = numericUpDown.Minimum;
                }
            }
        }


    }
}
