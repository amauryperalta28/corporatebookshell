﻿using CorporateBookShell.BusinessLogic;
using CorporateBookShell.Persistence.InMemory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CorporateBookShell.UI.Forms
{
    public partial class MainMenu : Form
    {
        private readonly Bookshelf bookshelf;

        public MainMenu()
        {
            InitializeComponent();
            var repository = new BookRepository();
            bookshelf = new Bookshelf(repository);
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BookEntry bookEntry = new BookEntry(bookshelf);
            
            bookEntry.MdiParent = this;
            bookEntry.Show();
        }
    }
}
