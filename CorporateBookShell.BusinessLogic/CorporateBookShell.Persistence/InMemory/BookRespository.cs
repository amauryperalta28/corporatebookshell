﻿using CorporateBookShell.BusinessLogic.Contracts;
using CorporateBookShell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorporateBookShell.Persistence.InMemory
{
   public class BookRepository : IBookRepository
    {
        IDictionary<string, Book> repository;

        public BookRepository()
        {
            this.repository = new Dictionary<string, Book>();
        }

        public bool Add(Book book)
        {
            repository.Add(book.ISBN, book);
            return true;
        }

        public bool Contains(Book book)
        {
            return repository.ContainsKey(book.ISBN);
        }
    }
}
