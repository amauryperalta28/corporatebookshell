﻿using CorporateBookShell.BusinessLogic;
using CorporateBookShell.BusinessLogic.Contracts;
using CorporateBookShell.Models;
using NUnit.Framework;

namespace CorporateBookShell.BussinessLogic.Tests
{
    [TestFixture]
    public class BookEntryTest
    {
        [TestCase("Platero y yo", "0001", "juan perez", 1992, TestName = "Case AddAValidBook 1")]
        [TestCase("El lazarillo", "0002", "anonimo", 1991, TestName = "Case AddAValidBook 2")]
        public void AddAValidBook(string title, string isbn, string author, int year)
        {
            //arrange
            Book book = new Book(title, isbn, author, year);
            IBookRepository bookRepository = new Persistence.InMemory.BookRepository();
            ///act
            var bookshelf = new Bookshelf(bookRepository);
            bool added = bookshelf.Add(book);
            ///assert
            Assert.AreEqual(true, added);
        }

        [TestCase("Clean Code", "0001", "Uncle Bob", 1991, false, TestName = "Case TestDuplicatedBook 1")]
        public void TestDuplicatedBook(string title, string isbn, string author, int year, bool expectedResult)
        {
            //arrange
            Book book = new Book(title, isbn, author, year);
            IBookRepository bookRepository = new Persistence.InMemory.BookRepository();
            var bookshelf = new Bookshelf(bookRepository);
            bookshelf.Add(book);

            ///act
            bool added = bookshelf.Add(book);
            ///assert
            Assert.AreEqual(expectedResult, added);
        }

        [TestCase("", "", "", 1990, TestName = "Case AddAnInvalidValidBook 1")]
        [TestCase("The art of unitTesting", "", "", 1990, TestName = "Case AddAnInvalidValidBook 2")]
        [TestCase("The art of unitTesting", "0004", "", 1990, TestName = "Case AddAnInvalidValidBook 3")]
        [TestCase("The art of unitTesting", "0004", "Pablo Neruda", 0, TestName = "Case AddAnInvalidValidBook 4")]
        [TestCase("", "0004", "Pablo Neruda", 1990, TestName = "Case AddAnInvalidValidBook 5")]
        [TestCase("   ", "0004", "Pablo Neruda", 1990, TestName = "Case AddAnInvalidValidBook 6")]
        [TestCase("   ", "   ", "   ", 1970, TestName = "Case AddAnInvalidValidBook 7")]
        public void AddAnInvalidValidBook(string title, string isbn, string author, int year)
        {
            //arrange
            Book book = new Book(title, isbn, author, year);
            IBookRepository bookRepository = new Persistence.InMemory.BookRepository();
            ///act
            var bookshelf = new Bookshelf(bookRepository);
            bool added = bookshelf.Add(book);
            ///assert
            Assert.AreEqual(false, added);
        }

        [TestCase(401,TestName = "Case AddAnInvalidBookWithPropertiesTooLong 401 ")]
        public void AddAnInvalidBookWithPropertiesTooLong(int length)
        {
            //arrange
            string title = "A".PadLeft(length, '0');
            string isbn = "1".PadLeft(length, 'd');
            string author = "j".PadLeft(length, 'w');
            int year = 1992;
            Book book = new Book(title, isbn, author, year);
            IBookRepository bookRepository = new Persistence.InMemory.BookRepository();
            
            ///act
            var bookshelf = new Bookshelf(bookRepository);
            bool added = bookshelf.Add(book);
            ///assert
            Assert.AreEqual(false, added);
        }







    }
}

