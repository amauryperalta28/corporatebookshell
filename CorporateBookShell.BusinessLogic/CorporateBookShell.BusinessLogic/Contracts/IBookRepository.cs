﻿using CorporateBookShell.Models;

namespace CorporateBookShell.BusinessLogic.Contracts
{
    /// <summary>
    /// This must be implemented for persistence books
    /// </summary>
    public interface IBookRepository
    {
        /// <summary>
        /// This must be implemented for add a book
        /// </summary>
        /// <param name="book">This represents a book</param>
        /// <returns>true if a book is added correctly</returns>
        bool Add(Book book);
        /// <summary>
        /// jhkjhkj
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        bool Contains(Book book);
    }
}