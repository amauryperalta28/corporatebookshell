﻿using CorporateBookShell.Models;
using System;
using CorporateBookShell.BusinessLogic.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace CorporateBookShell.BusinessLogic
{
    /// <summary>
    /// Represents a bookshelf
    /// </summary>
    public class Bookshelf
    {
        private const int MaximumStringPropertiesLength = 400;
        private IBookRepository bookRepository;

        public Bookshelf(IBookRepository bookRepository)
        {
            this.bookRepository = bookRepository;
        }

        /// <summary>
        /// adds a book to the bookshelf
        /// </summary>
        /// <param name="book">book to added</param>
        /// <returns>true if the book is added; false if the book was already added</returns>
        public bool Add(Book book)
        {
            if (!ValidateBook(book))
            {
                return false;
            }

            return bookRepository.Add(book);
        }

        private bool ValidateBook(Book book)
        {

            if (bookRepository.Contains(book))
            {
                return false;
            }


            if (String.IsNullOrWhiteSpace(book.Title) || String.IsNullOrWhiteSpace(book.ISBN) || String.IsNullOrWhiteSpace(book.Author) || book.Year <= 0)
            {
                return false;
            }

            if (ValidatePropertiesLength(book))
            {
                return false;
            }

            return true;
        }

        private bool ValidatePropertiesLength(Book book)
        {
            List<string> properties = new List<string> { book.Author, book.Title, book.ISBN };

            return properties.Any(prop => prop.Length > MaximumStringPropertiesLength);
        }
    }
}