﻿using System.Collections.Generic;

namespace CorporateBookShell.Models
{
    /// <summary>
    /// Represents a book entity
    /// </summary>
    public class Book
    {

        public Book(string title, string isbn, string author, int year)
        {
            Title = title;
            ISBN = isbn;
            Author = author;
            Year = year;
        }

        public string ISBN { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int Year { get; set; }
        /// <summary>
        /// Information to aid search.
        /// </summary>
        public ISet<string>  Tags { get; set; }
    }
}
